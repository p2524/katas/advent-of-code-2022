import { expect } from 'chai'
import { moveStack, getOrderFromLine, getTopCrates9001FromFile, getTopCratesFromFile, parseLineOfStacks, parseFileStackAndOrders, getTopCreatesFromStack, moveStack9001 } from '../src/05-supply-stack.js'

const initialTestStackMap = {
  1: ['N', 'Z'],
  2: ['D', 'C', 'M'],
  3: ['P']
}
const testFilePath = 'test/day_5/example.txt'
const inputFilePath = 'resources/day_5/input.txt'

describe('Supply Stacks for day 5', () => {
  describe('Part I', () => {
    describe('Move in the stack by orders', () => {
      const initialTestStackMap = {
        1: ['N', 'Z'],
        2: ['D', 'C', 'M'],
        3: ['P']
      }
      it('Should move 1 from 2 to 1', () => {
        const testinitialTestStackMap = moveStack([{ numberElements: 1, lineNumberFrom: 2, lineNumberTo: 1 }], initialTestStackMap)

        expect(testinitialTestStackMap[1]).to.be.deep.equal(['D', 'N', 'Z'])
        expect(testinitialTestStackMap[2]).to.be.deep.equal(['C', 'M'])
        expect(testinitialTestStackMap[3]).to.be.deep.equal(['P'])
      })
      it('Should move 1 from 2 to 1 and move 3 from 1 to 3', () => {
        const testinitialTestStackMap = moveStack([
          { numberElements: 1, lineNumberFrom: 2, lineNumberTo: 1 },
          { numberElements: 3, lineNumberFrom: 1, lineNumberTo: 3 }
        ], initialTestStackMap)

        expect(testinitialTestStackMap[1]).to.be.deep.equal([])
        expect(testinitialTestStackMap[2]).to.be.deep.equal(['C', 'M'])
        expect(testinitialTestStackMap[3]).to.be.deep.equal(['Z', 'N', 'D', 'P'])
      })
      it('Should move 1 from 2 to 1, move 3 from 1 to 3 and move 2 from 2 to 1', () => {
        const testinitialTestStackMap = moveStack([
          { numberElements: 1, lineNumberFrom: 2, lineNumberTo: 1 },
          { numberElements: 3, lineNumberFrom: 1, lineNumberTo: 3 },
          { numberElements: 2, lineNumberFrom: 2, lineNumberTo: 1 }
        ], initialTestStackMap)

        expect(testinitialTestStackMap[1]).to.be.deep.equal(['M', 'C'])
        expect(testinitialTestStackMap[2]).to.be.deep.equal([])
        expect(testinitialTestStackMap[3]).to.be.deep.equal(['Z', 'N', 'D', 'P'])
      })
      it('Should move 1 from 2 to 1, move 3 from 1 to 3, move 2 from 2 to 1 and move 1 from 1 to 2', () => {
        const testinitialTestStackMap = moveStack([
          { numberElements: 1, lineNumberFrom: 2, lineNumberTo: 1 },
          { numberElements: 3, lineNumberFrom: 1, lineNumberTo: 3 },
          { numberElements: 2, lineNumberFrom: 2, lineNumberTo: 1 },
          { numberElements: 1, lineNumberFrom: 1, lineNumberTo: 2 }
        ], initialTestStackMap)

        expect(testinitialTestStackMap[1]).to.be.deep.equal(['C'])
        expect(testinitialTestStackMap[2]).to.be.deep.equal(['M'])
        expect(testinitialTestStackMap[3]).to.be.deep.equal(['Z', 'N', 'D', 'P'])
      })
    })
    describe('Parse line of moves', () => {
      it('Should create object from line move 1 from 2 to 1', () => {
        const result = getOrderFromLine('move 1 from 2 to 1')

        expect(result).to.be.deep.equal({ numberElements: 1, lineNumberFrom: 2, lineNumberTo: 1 })
      })
      it('Should create object from line move 11 from 6 to 5', () => {
        const result = getOrderFromLine('move 11 from 6 to 5')

        expect(result).to.be.deep.equal({ numberElements: 11, lineNumberFrom: 6, lineNumberTo: 5 })
      })
    })
    describe('Parse line of crates', () => {
      it('Parse first line', () => {
        const initialStack = {}
        const line = '    [D]    '

        const newStack = parseLineOfStacks(line, initialStack)

        expect(newStack[2]).to.be.deep.equal(['D'])
      })
      it('Parse second line of crates', () => {
        const initialStack = {
          1: [],
          2: ['D'],
          3: []
        }
        const line = '[N] [C]    '

        const newStack = parseLineOfStacks(line, initialStack)

        expect(newStack[1]).to.be.deep.equal(['N'])
        expect(newStack[2]).to.be.deep.equal(['D', 'C'])
      })
      it('Parse third line of crates', () => {
        const initialStack = {
          1: ['N'],
          2: ['D', 'C'],
          3: []
        }
        const line = '[Z] [M] [P]'

        const newStack = parseLineOfStacks(line, initialStack)

        expect(newStack[1]).to.be.deep.equal(['N', 'Z'])
        expect(newStack[2]).to.be.deep.equal(['D', 'C', 'M'])
        expect(newStack[3]).to.be.deep.equal(['P'])
      })
    })
    describe('Parse all the info before moving', () => {
      it('Parse all the example file', async () => {
        const { initialStack, orders } = await parseFileStackAndOrders(testFilePath)

        expect(initialStack).to.be.deep.equal(initialTestStackMap)
        expect(orders).to.be.deep.equal([
          { numberElements: 1, lineNumberFrom: 2, lineNumberTo: 1 },
          { numberElements: 3, lineNumberFrom: 1, lineNumberTo: 3 },
          { numberElements: 2, lineNumberFrom: 2, lineNumberTo: 1 },
          { numberElements: 1, lineNumberFrom: 1, lineNumberTo: 2 }
        ])
      })
      it('Get the stack after all the orders are executed in the example file', async () => {
        const { initialStack, orders } = await parseFileStackAndOrders(testFilePath)

        const finalStack = moveStack(orders, initialStack)

        expect(finalStack).to.be.deep.equal({
          1: ['C'],
          2: ['M'],
          3: ['Z', 'N', 'D', 'P']
        })
      })
      it('Get the first elements from new stack after orders', async () => {
        const { initialStack, orders } = await parseFileStackAndOrders(testFilePath)

        const finalStack = moveStack(orders, initialStack)
        const topCrates = getTopCreatesFromStack(finalStack)

        expect(topCrates).to.be.equal('CMZ')
      })
      it('Get the first elements from new stack after orders all together', async () => {
        const topCrates = await getTopCratesFromFile(testFilePath)

        expect(topCrates).to.be.equal('CMZ')
      })
      it('Get the first elements from new stack after orders all together normal file', async () => {
        const topCrates = await getTopCratesFromFile(inputFilePath)

        expect(topCrates).to.be.equal('QMBMJDFTD')
      })
    })
  })
  describe('Part two, CrateMover 9001', () => {
    describe('Move in the stack by orders', () => {
      const initialTestStackMap = {
        1: ['N', 'Z'],
        2: ['D', 'C', 'M'],
        3: ['P']
      }
      it('Should move 1 from 2 to 1', () => {
        const testinitialTestStackMap = moveStack9001([{ numberElements: 1, lineNumberFrom: 2, lineNumberTo: 1 }], initialTestStackMap)

        expect(testinitialTestStackMap[1]).to.be.deep.equal(['D', 'N', 'Z'])
        expect(testinitialTestStackMap[2]).to.be.deep.equal(['C', 'M'])
        expect(testinitialTestStackMap[3]).to.be.deep.equal(['P'])
      })
      it('Should move 1 from 2 to 1 and move 3 from 1 to 3', () => {
        const testinitialTestStackMap = moveStack9001([
          { numberElements: 1, lineNumberFrom: 2, lineNumberTo: 1 },
          { numberElements: 3, lineNumberFrom: 1, lineNumberTo: 3 }
        ], initialTestStackMap)

        expect(testinitialTestStackMap[1]).to.be.deep.equal([])
        expect(testinitialTestStackMap[2]).to.be.deep.equal(['C', 'M'])
        expect(testinitialTestStackMap[3]).to.be.deep.equal(['D', 'N', 'Z', 'P'])
      })
      it('Should move 1 from 2 to 1, move 3 from 1 to 3 and move 2 from 2 to 1', () => {
        const testinitialTestStackMap = moveStack9001([
          { numberElements: 1, lineNumberFrom: 2, lineNumberTo: 1 },
          { numberElements: 3, lineNumberFrom: 1, lineNumberTo: 3 },
          { numberElements: 2, lineNumberFrom: 2, lineNumberTo: 1 }
        ], initialTestStackMap)

        expect(testinitialTestStackMap[1]).to.be.deep.equal(['C', 'M'])
        expect(testinitialTestStackMap[2]).to.be.deep.equal([])
        expect(testinitialTestStackMap[3]).to.be.deep.equal(['D', 'N', 'Z', 'P'])
      })
      it('Should move 1 from 2 to 1, move 3 from 1 to 3, move 2 from 2 to 1 and move 1 from 1 to 2', () => {
        const testinitialTestStackMap = moveStack9001([
          { numberElements: 1, lineNumberFrom: 2, lineNumberTo: 1 },
          { numberElements: 3, lineNumberFrom: 1, lineNumberTo: 3 },
          { numberElements: 2, lineNumberFrom: 2, lineNumberTo: 1 },
          { numberElements: 1, lineNumberFrom: 1, lineNumberTo: 2 }
        ], initialTestStackMap)

        expect(testinitialTestStackMap[1]).to.be.deep.equal(['M'])
        expect(testinitialTestStackMap[2]).to.be.deep.equal(['C'])
        expect(testinitialTestStackMap[3]).to.be.deep.equal(['D', 'N', 'Z', 'P'])
      })
    })
    describe('Movement with new crater', () => {
      it('Get the first elements from new stack after orders all together 9001', async () => {
        const topCrates = await getTopCrates9001FromFile(testFilePath)

        expect(topCrates).to.be.equal('MCD')
      })
      it('Get the first elements from new stack after orders all together normal file 9001', async () => {
        const topCrates = await getTopCrates9001FromFile(inputFilePath)

        expect(topCrates).to.be.equal('NBTVTJNFJ')
      })
    })
  })
})
