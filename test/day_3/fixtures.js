const useCases = [
  {
    line: 1,
    lineOfElements: 'vJrwpWtwJgWrhcsFMMfFFhFp',
    result: 16
  },
  {
    line: 2,
    lineOfElements: 'jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL',
    result: 38
  },
  {
    line: 3,
    lineOfElements: 'PmmdzqPrVvPwwTWBwg',
    result: 42
  },
  {
    line: 4,
    lineOfElements: 'wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn',
    result: 22
  },
  {
    line: 5,
    lineOfElements: 'ttgJtRGJQctTZtZT',
    result: 20
  },
  {
    line: 6,
    lineOfElements: 'CrZsJsPPZsGzwwsLwLmpwMDw',
    result: 19
  }
]

export { useCases }
