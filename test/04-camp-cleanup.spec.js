import { expect } from 'chai'
import { checkFullyContains, getListsFromLine, getFullyOverlaps, checkOverlap, getOverlaps } from '../src/04-camp-cleanup.js'

const testFilePath = 'test/day_4/example.txt'
const inputFIlePath = 'resources/day_4/input.txt'

describe('Camp clean up tests', () => {
  describe('Part 1', () => {
    describe('Check contains method', () => {
      it('Check 1, 2 fully contains 1', () => {
        const result = checkFullyContains([1, 2], [1])

        expect(result).to.be.equal(true)
      })
      it('Check 1 is fully contained by 1, 2', () => {
        const result = checkFullyContains([1], [1, 2])

        expect(result).to.be.equal(true)
      })
      it('Check 1, 2 does not fully contains 1, 3', () => {
        const result = checkFullyContains([1, 2], [1, 3])

        expect(result).to.be.equal(false)
      })
      it('Check 2-8 fully contains 3,7', () => {
        const result = checkFullyContains([2, 3, 4, 5, 6, 7, 8], [3, 4, 5, 6, 7])

        expect(result).to.be.equal(true)
      })
    })
    describe('Parse lines of the puzzle', () => {
      it('2-4,6-8', () => {
        const [list1, list2] = getListsFromLine('2-4,6-8')

        expect(list1).to.be.deep.equal([2, 3, 4])
        expect(list2).to.be.deep.equal([6, 7, 8])
      })
      it('2-3,4-5', () => {
        const [list1, list2] = getListsFromLine('2-3,4-5')

        expect(list1).to.be.deep.equal([2, 3])
        expect(list2).to.be.deep.equal([4, 5])
      })
      it('2-8,3-7', () => {
        const [list1, list2] = getListsFromLine('2-8,3-7')

        expect(list1).to.be.deep.equal([2, 3, 4, 5, 6, 7, 8])
        expect(list2).to.be.deep.equal([3, 4, 5, 6, 7])
      })
    })
    describe('Parse file', () => {
      it('Should get two elements from example file', async () => {
        const totalOverlaps = await getFullyOverlaps(testFilePath)

        expect(totalOverlaps).to.be.equal(2)
      })
      it('Should get two elements from input file', async () => {
        const totalOverlaps = await getFullyOverlaps(inputFIlePath)

        expect(totalOverlaps).to.be.equal(441)
      })
    })
  })
  describe('Part two all the overlapping', () => {
    it('Check overlap 1, 2 with 2, 3', () => {
      const result = checkOverlap([1, 2], [2, 3])

      expect(result).to.be.equal(true)
    })
    it('Check overlap 1, 2 with 3, 4', () => {
      const result = checkOverlap([1, 2], [3, 4])

      expect(result).to.be.equal(false)
    })
    it('Should get four elements from example file', async () => {
      const totalOverlaps = await getOverlaps(testFilePath)

      expect(totalOverlaps).to.be.equal(4)
    })
    it('Should get four elements from input file', async () => {
      const totalOverlaps = await getOverlaps(inputFIlePath)

      expect(totalOverlaps).to.be.equal(861)
    })
  })
})
