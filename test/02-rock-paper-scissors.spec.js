import { expect } from 'chai'
import { calculateRound, getListOfRoundsFromFile, getResultFromArrayOfRounds, calculateResultFromFilePath, calculateRound2 } from '../src/02-rock-paper-scissors.js'

const testFilePath = 'test/day_2/example.txt'

describe('Testing the rock, paper, scissors', () => {
  describe('Calculate round use cases', () => {
    it('Given A Y should return 8 (2 paper + 6 winning)', () => {
      const roundResult = calculateRound('A', 'Y')
      expect(roundResult).to.be.equal(8)
    })
    it('Given B X should return 1 (1 rock + 0 losing)', () => {
      const roundResult = calculateRound('B', 'X')
      expect(roundResult).to.be.equal(1)
    })
    it('Given C Z should return 6 (3 scissors + 3 draw)', () => {
      const roundResult = calculateRound('C', 'Z')
      expect(roundResult).to.be.equal(6)
    })
  })
  describe('Get the input data', () => {
    it('Given input file should return list of hands', async () => {
      const listOfRounds = await getListOfRoundsFromFile(testFilePath)

      expect(listOfRounds).to.be.deep.equal([['C', 'Z'], ['C', 'Z'], ['A', 'Y']])
    })
  })
  describe('Given list of data should return result', () => {
    it("['C', 'Z'] should return 6", () => {
      const result = getResultFromArrayOfRounds([['C', 'Z']])

      expect(result).to.be.equal(6)
    })
    it("['C', 'Z'] twice should return 12", () => {
      const result = getResultFromArrayOfRounds([['C', 'Z'], ['C', 'Z']])

      expect(result).to.be.equal(12)
    })
    it("['C', 'Z'] twice and ['A', 'Y'] should return 20", () => {
      const result = getResultFromArrayOfRounds([['C', 'Z'], ['C', 'Z'], ['A', 'Y']])

      expect(result).to.be.equal(20)
    })
  })
  describe('Calculate result from input data', () => {
    it('Given test example file should return 20', async () => {
      const result = await calculateResultFromFilePath(testFilePath)

      expect(result).to.be.equal(20)
    })
    it('Given input file should return 12156', async () => {
      const result = await calculateResultFromFilePath('resources/day_2/input.txt')

      expect(result).to.be.equal(12156)
    })
  })
})

describe('Part two', () => {
  describe('Calculate round use cases', () => {
    it('Given A Y should return 4 (1 rock + 3 draw)', () => {
      const roundResult = calculateRound2('A', 'Y')
      expect(roundResult).to.be.equal(4)
    })
    it('Given B X should return 1 (1 rock + 0 losing)', () => {
      const roundResult = calculateRound2('B', 'X')
      expect(roundResult).to.be.equal(1)
    })
    it('Given C Z should return 7 (1 rock + 6 win)', () => {
      const roundResult = calculateRound2('C', 'Z')
      expect(roundResult).to.be.equal(7)
    })
  })
  describe('Calculate result from input data', () => {
    it('Given test example file should return 20', async () => {
      const result = await calculateResultFromFilePath(testFilePath)

      expect(result).to.be.equal(20)
    })
    it('Given input file should return 10835', async () => {
      const result = await calculateResultFromFilePath('resources/day_2/input.txt', calculateRound2)

      expect(result).to.be.equal(10835)
    })
  })
})
