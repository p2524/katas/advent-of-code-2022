const inLineFixtures = [
  {
    testName: 'Given your line and position should return correct visibility 0',
    inputLineOfTress: [1, 2, 3, 4, 4, 5, 1, 0],
    position: 0,
    isVisible: true
  },
  {
    testName: 'Given your line and position should return correct visibility 1',
    inputLineOfTress: [1, 2, 3, 4, 4, 5, 1, 0],
    position: 1,
    isVisible: true
  },
  {
    testName: 'Given your line and position should return correct visibility 2',
    inputLineOfTress: [1, 2, 3, 4, 4, 5, 1, 0],
    position: 2,
    isVisible: true
  },
  {
    testName: 'Given your line and position should return correct visibility 3',
    inputLineOfTress: [1, 2, 3, 4, 4, 5, 1, 0],
    position: 3,
    isVisible: true
  },
  {
    testName: 'Given your line and position should return correct visibility 4',
    inputLineOfTress: [1, 2, 3, 4, 4, 5, 1, 0],
    position: 4,
    isVisible: false
  },
  {
    testName: 'Given your line and position should return correct visibility 5',
    inputLineOfTress: [1, 2, 3, 4, 4, 5, 1, 0],
    position: 5,
    isVisible: true
  },
  {
    testName: 'Given your line and position should return correct visibility 6',
    inputLineOfTress: [1, 2, 3, 4, 4, 5, 1, 0],
    position: 6,
    isVisible: true
  },
  {
    testName: 'Given your line and position should return correct visibility edge',
    inputLineOfTress: [1, 2, 3, 4, 4, 5, 1, 0],
    position: 7,
    isVisible: true
  }
]

export { inLineFixtures }
