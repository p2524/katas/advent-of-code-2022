import { expect } from 'chai'
import { isAMarker, getFirstMarkerPosition, getFirstMarker, getFirstMarkerMessagePosition, getFirstMessageMarker } from '../src/06-tuning-trouble.js'
import { dataStreamBufferUseCases, fourLetterUseCases } from './day_6/fixtures.js'

const inputFilePath = 'resources/day_6/input.txt'

describe('Day 6: Tuning trouble', () => {
  describe('Part I', () => {
    fourLetterUseCases.forEach(({ input, expected }) => {
      it(`Given ${input} should return ${expected}`, () => {
        const result = isAMarker(input)

        expect(result).to.be.equal(expected)
      })
    })
    describe('Getting the first marker from string', () => {
      dataStreamBufferUseCases.forEach(({ input, expected }) => {
        it(`Given ${input} should say ${expected}`, () => {
          const result = getFirstMarkerPosition(input)

          expect(result).to.be.equal(expected)
        })
      })
    })
    describe('Getting the market from input', () => {
      it('Should get marker number from file', async () => {
        const result = await getFirstMarker(inputFilePath)

        expect(result).to.be.equal(1100)
      })
    })
  })
  describe('Part two, get the message marker', () => {
    describe('Getting the first message marker from string', () => {
      dataStreamBufferUseCases.forEach(({ input, expectedMessage }) => {
        it(`Given ${input} should say ${expectedMessage}`, () => {
          const result = getFirstMarkerMessagePosition(input)

          expect(result).to.be.equal(expectedMessage)
        })
      })
    })
    describe('Getting the market from input', () => {
      it('Should get marker number from file', async () => {
        const result = await getFirstMessageMarker(inputFilePath)

        expect(result).to.be.equal(2421)
      })
    })
  })
})
