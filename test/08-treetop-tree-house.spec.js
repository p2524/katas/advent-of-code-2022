import { expect } from 'chai'
import { getVisibleTrees, isTreeVisibleInLine, getMaxScenicScore } from '../src/08-treetop-tree-house.js'
import { inLineFixtures } from './day_8/fixtures.js'

const testInputFilePath = 'test/day_8/example.txt'
const filePath = 'resources/day_8/input.txt'

describe('Day 8 challenge', () => {
  describe('Part I how many trees are visible from outside the grid?', () => {
    it('Given example input should return 21 visible tress', async () => {
      const result = await getVisibleTrees(testInputFilePath)

      expect(result).to.be.equal(21)
    })
    it('Given normal input should return 1681 visible tress', async () => {
      const result = await getVisibleTrees(filePath)

      expect(result).to.be.equal(1681)
    })
    inLineFixtures.forEach(({ testName, inputLineOfTress, position, isVisible }) => {
      it(testName, () => {
        const result = isTreeVisibleInLine(inputLineOfTress, position)

        expect(result).to.be.equal(isVisible)
      })
    })
  })
  describe('Part II. What is the highest scenic score possible for any tree?', () => {
    it('Given example input should return 8 high scenic score', async () => {
      const result = await getMaxScenicScore(testInputFilePath)

      expect(result).to.be.equal(8)
    })
    it('Given normal input should return 201684 high scenic score', async () => {
      const result = await getMaxScenicScore(filePath)

      expect(result).to.be.equal(201684)
    })
  })
})
