const fourLetterUseCases = [
  {
    input: 'mjqj',
    expected: false
  },
  {
    input: 'jqjp',
    expected: false
  },
  {
    input: 'qjpq',
    expected: false
  },
  {
    input: 'jpqm',
    expected: true
  }
]

const dataStreamBufferUseCases = [
  {
    input: 'mjqjpqmgbljsphdztnvjfqwrcgsmlb',
    expected: 7,
    expectedMessage: 19
  },
  {
    input: 'bvwbjplbgvbhsrlpgdmjqwftvncz',
    expected: 5,
    expectedMessage: 23
  },
  {
    input: 'nppdvjthqldpwncqszvftbrmjlhg',
    expected: 6,
    expectedMessage: 23
  },
  {
    input: 'nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg',
    expected: 10,
    expectedMessage: 29
  },
  {
    input: 'zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw',
    expected: 11,
    expectedMessage: 26
  },
  {
    input: 'aaaaaaaaaaaa',
    expected: -1,
    expectedMessage: -1
  }
]

export { fourLetterUseCases, dataStreamBufferUseCases }
