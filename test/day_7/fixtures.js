const example1Tree = {
  positioned: true,
  name: '/',
  type: 'dir',
  childs: [{
    type: 'file',
    name: 'a.txt',
    size: 1
  },
  {
    type: 'file',
    name: 'b.txt',
    size: 2
  }]
}

const cdTree = {
  positioned: true,
  name: '/',
  type: 'dir'
}

const cdAndLsTree = {
  positioned: true,
  name: '/',
  type: 'dir',
  childs: [{
    name: 'a',
    type: 'dir',
    positioned: false
  }, {
    name: 'b.txt',
    type: 'file',
    size: 14848514
  }, {
    name: 'c.dat',
    type: 'file',
    size: 8504156
  }, {
    name: 'd',
    type: 'dir',
    positioned: false
  }]
}

const cdLscdATree = {
  name: '/',
  type: 'dir',
  positioned: false,
  childs: [{
    name: 'a',
    type: 'dir',
    positioned: true
  }, {
    name: 'b.txt',
    type: 'file',
    size: 14848514
  }, {
    name: 'c.dat',
    type: 'file',
    size: 8504156
  }, {
    name: 'd',
    type: 'dir',
    positioned: false
  }]
}

const finalTreeExample = {
  name: '/',
  type: 'dir',
  positioned: false,
  childs: [{
    name: 'a',
    type: 'dir',
    positioned: true,
    childs: [
      {
        name: 'e',
        type: 'dir',
        positioned: false
      },
      {
        name: 'f',
        type: 'file',
        size: 29116
      },
      {
        name: 'g',
        type: 'file',
        size: 2557
      },
      {
        name: 'h.lst',
        type: 'file',
        size: 62596
      }

    ]
  }, {
    name: 'b.txt',
    type: 'file',
    size: 14848514
  }, {
    name: 'c.dat',
    type: 'file',
    size: 8504156
  }, {
    name: 'd',
    type: 'dir',
    positioned: false
  }]
}

export { example1Tree, cdTree, cdAndLsTree, cdLscdATree, finalTreeExample }
