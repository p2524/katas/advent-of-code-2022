import { expect } from 'chai'
import { getTreeFromFile, calculateSizeOfDir, processCommand, processList, getDirPositionedInTree, getSizeOfDirectories, findDirectoriesBySize, findSmallDirectoryToFreeSpace } from '../src/07-no-space-left.js'
import { example1Tree, cdTree, cdAndLsTree, cdLscdATree, finalTreeExample } from './day_7/fixtures.js'

const testFilePath1 = 'test/day_7/example_1.txt'
const testFilePath2 = 'test/day_7/example_2.txt'
const inputFIlePath = 'resources/day_7/input.txt'

describe('Day 7: No space left on device', () => {
  describe('Part I', () => {
    it('Given example file 1 should return 1 directory with 2 files', async () => {
      const result = await getTreeFromFile(testFilePath1)

      expect(result).to.be.deep.equal(example1Tree)
    })
    it('Given root tree should be able to calculate the size of the directories', () => {
      const result = getSizeOfDirectories(example1Tree)

      expect(result).to.be.deep.equal({
        positioned: true,
        name: '/',
        type: 'dir',
        size: 3,
        childs: [{
          type: 'file',
          name: 'a.txt',
          size: 1
        },
        {
          type: 'file',
          name: 'b.txt',
          size: 2
        }]
      })
    })
    describe('Calculate size of directory', () => {
      it('Given only one directory, should calculate the size of it', () => {
        const directory = {
          name: 'my_directory',
          type: 'dir',
          childs: [{
            name: 'file1.txt',
            size: 12345
          }, {
            name: 'file2.txt',
            size: 12345
          }]
        }

        const sizeOfDir = calculateSizeOfDir(directory)

        expect(sizeOfDir).to.be.equal(24690)
      })
      it('Given directory with one subdirectory, should calculate size', () => {
        const directory = {
          name: 'my_directory',
          type: 'dir',
          childs: [{
            name: 'file1.txt',
            size: 12345
          }, {
            name: 'file2.txt',
            size: 12345
          },
          {
            name: 'my_sub_directory',
            type: 'dir',
            childs: [{
              name: 'file1.txt',
              size: 12345
            }, {
              name: 'file2.txt',
              size: 12345
            }]
          }]
        }

        const sizeOfDir = calculateSizeOfDir(directory)

        expect(sizeOfDir).to.be.equal(2 * 24690)
      })
    })
    describe('Process commands one at a time', () => {
      it('Given cd / should create root dir empty', () => {
        const tree = processCommand('$ cd /', {})

        expect(tree).to.be.deep.equal(cdTree)
      })
      it('Given cd / and ls should create root childs', () => {
        const tree = processCommand('$ cd /', {})

        const newTree = processList(['dir a', '14848514 b.txt', '8504156 c.dat', 'dir d'], tree)

        expect(newTree).to.be.deep.equal(cdAndLsTree)
      })
      it('Given cd / and ls cd cd a should positionate on a', () => {
        const tree = processCommand('$ cd /', {})
        const newTree = processList(['dir a', '14848514 b.txt', '8504156 c.dat', 'dir d'], tree)
        const finalTree = processCommand('$ cd a', newTree)

        expect(finalTree).to.be.deep.equal(cdLscdATree)
      })
      it('Given cd / and ls cd cd a and ls should create the childs on a', () => {
        const tree = processCommand('$ cd /', {})
        const newTree = processList(['dir a', '14848514 b.txt', '8504156 c.dat', 'dir d'], tree)
        const newTree2 = processCommand('$ cd a', newTree)
        const finalTree = processList(['dir e', '29116 f', '2557 g', '62596 h.lst'], newTree2)

        expect(finalTree).to.be.deep.equal(finalTreeExample)
      })
      it('Get dir positioned', () => {
        const tree = {
          childs: [
            {
              childs: [
                {
                  positioned: true,
                  childs: [1, 2, 3, 4, 5]
                }
              ]
            }
          ]
        }

        const dirPositioned = getDirPositionedInTree(tree)

        expect(dirPositioned).to.be.deep.equal({
          positioned: true,
          childs: [1, 2, 3, 4, 5]
        })
      })
    })
  })
})

describe('Process file for real', () => {
  describe('Part I', () => {
    it('Get tree from file', async () => {
      const tree = await getTreeFromFile(testFilePath2)

      expect(tree.positioned).to.be.equal(true)
    })
    it('Get tree from file and calculate size directories', async () => {
      const tree = await getTreeFromFile(testFilePath2)

      const directoriesSize = getSizeOfDirectories(tree)

      expect(directoriesSize.size).to.be.equal(48381165)
    })
    it('Get the directories with at most 100000', async () => {
      const tree = await getTreeFromFile(testFilePath2)

      const directories = findDirectoriesBySize(100000, tree)

      expect(directories).to.be.deep.equal({ directories: [{ name: 'a', size: 94853 }, { name: 'e', size: 584 }], total: 95437 })
    })
    it('Get the directories with at most 100000 input file', async () => {
      const tree = await getTreeFromFile(inputFIlePath)

      const directories = findDirectoriesBySize(100000, tree)

      expect(directories.total).to.be.equal(1648397)
    })
  })
  describe('Part II. Get the smallest directory to delete and have enough space to run the update', () => {
    it('Given example input file should return directory d with 24933642', async () => {
      const tree = await getTreeFromFile(testFilePath2)

      const smallestDirectory = findSmallDirectoryToFreeSpace({ total: 70000000, needFree: 30000000 }, tree)

      expect(smallestDirectory).to.be.deep.equal({
        name: 'd',
        size: 24933642
      })
    })
    it('Given real input file should return directory d with 24933642', async () => {
      const tree = await getTreeFromFile(inputFIlePath)

      const smallestDirectory = findSmallDirectoryToFreeSpace({ total: 70000000, needFree: 30000000 }, tree)

      expect(smallestDirectory).to.be.deep.equal({
        name: 'nbq',
        size: 1815525
      })
    })
  })
})
