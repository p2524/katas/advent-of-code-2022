import { expect } from 'chai'
import { getMaxCaloriesFromFile, getThreeMaxCalories, parseFileDataToArray } from '../src/01-calories.js'

describe('Getting the elph with more calories', () => {
  it('Given example from test should return 24000 calories', async () => {
    const filePath = 'test/example.txt'

    const maxCalories = await getMaxCaloriesFromFile(filePath)

    expect(maxCalories).to.be.equal(24000)
  })
  it('Given example from test should return 45000 calories (top three)', async () => {
    const filePath = 'test/example.txt'

    const maxCalories = await getThreeMaxCalories(filePath)

    expect(maxCalories).to.be.equal(45000)
  })
  it('Given input from test should return 196804 calories (top three)', async () => {
    const filePath = 'test/input.txt'

    const maxCalories = await getThreeMaxCalories(filePath)

    expect(maxCalories).to.be.equal(196804)
  })
  it('Given example from test should return 66186 calories', async () => {
    const filePath = 'test/input.txt'

    const maxCalories = await getMaxCaloriesFromFile(filePath)

    expect(maxCalories).to.be.equal(66186)
  })
  it('Given two elfs shoud return the calories of the two', () => {
    const stringEntry = '100\n1\n\n200\n2'

    const result = parseFileDataToArray(stringEntry)

    expect(result).to.be.deep.equal([101, 202])
  })
  it('Given three elfs shoud return tthe calories of the three', () => {
    const stringEntry = '100\n1\n\n200\n2\n\n300\n3'

    const result = parseFileDataToArray(stringEntry)

    expect(result).to.be.deep.equal([101, 202, 303])
  })
})
