import { expect } from 'chai'
import {
  getIntersect,
  getListsFromString,
  getIntersectionFromBag,
  getPriorityFromOneLine,
  getPrioritiesFromFile,
  getBadgeFromElfs,
  getBadgePrioritiesFromFilePath,
  getSeparatedByThree
} from '../src/03-rucksack-reorganization.js'
import { useCases } from './day_3/fixtures.js'

const testFilePath = 'test/day_3/example.txt'
const inputFilePath = 'resources/day_3/input.txt'

describe('Get priorities', () => {
  describe('Get array intersections', () => {
    it('First case', () => {
      const intersect = getIntersect([1, 2, 3, 4], [3, 4, 5, 6])

      expect(intersect).to.be.deep.equal([3, 4])
    })
  })
  describe('Process one line', () => {
    it('Get lists from one line', () => {
      const [list1, list2] = getListsFromString('vJrwpWtwJgWrhcsFMMfFFhFp')

      expect(list1).to.be.deep.equal('vJrwpWtwJgWr'.split(''))
      expect(list2).to.be.deep.equal('hcsFMMfFFhFp'.split(''))
    })
    it('Get intersection from one line', () => {
      const intersection = getIntersectionFromBag('vJrwpWtwJgWrhcsFMMfFFhFp')

      expect(intersection).to.be.deep.equal(['p'])
    })
    it('Get intersection from line 2', () => {
      const intersection = getIntersectionFromBag(
        'jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL'
      )

      expect(intersection).to.be.deep.equal(['L'])
    })
    describe('Priorities', () => {
      useCases.map((useCase) =>
        it(`Get priority from line ${useCase.line}`, () => {
          const priority = getPriorityFromOneLine(useCase.lineOfElements)

          expect(priority).to.be.equal(useCase.result)
        })
      )
    })
    describe('Priorities from files', () => {
      it('Should get 157 from example file', async () => {
        const priorities = await getPrioritiesFromFile(testFilePath)

        expect(priorities).to.be.equal(157)
      })
      it('Should get correct number from real file', async () => {
        const priorities = await getPrioritiesFromFile(inputFilePath)

        expect(priorities).to.be.equal(7845)
      })
    })
  })
})

describe('Part two', () => {
  it('Get intersection from three lines', () => {
    const input = [
      'vJrwpWtwJgWrhcsFMMfFFhFp',
      'jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL',
      'PmmdzqPrVvPwwTWBwg'
    ]

    const badge = getBadgeFromElfs(input)

    expect(badge).to.be.deep.equal(['r'])
  })
  it('Get intersection from three lines 2', () => {
    const input = [
      'wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn',
      'ttgJtRGJQctTZtZT',
      'CrZsJsPPZsGzwwsLwLmpwMDw'
    ]

    const badge = getBadgeFromElfs(input)

    expect(badge).to.be.deep.equal(['Z'])
  })

  it('Split in badges of three', () => {
    const input = ['vJrwpWtwJgWrhcsFMMfFFhFp',
      'jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL',
      'PmmdzqPrVvPwwTWBwg', 'wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn',
      'ttgJtRGJQctTZtZT',
      'CrZsJsPPZsGzwwsLwLmpwMDw']

    const result = getSeparatedByThree(input)

    expect(result).to.be.deep.equal([
      [
        'vJrwpWtwJgWrhcsFMMfFFhFp',
        'jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL',
        'PmmdzqPrVvPwwTWBwg'
      ],
      [
        'wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn',
        'ttgJtRGJQctTZtZT',
        'CrZsJsPPZsGzwwsLwLmpwMDw'
      ]
    ])
  })

  it('Get priorities from badges example', async () => {
    const priorities = await getBadgePrioritiesFromFilePath(testFilePath)

    expect(priorities).to.be.equal(70)
  })

  it('Get priorities from badges input', async () => {
    const priorities = await getBadgePrioritiesFromFilePath(inputFilePath)

    expect(priorities).to.be.equal(2790)
  })
})
