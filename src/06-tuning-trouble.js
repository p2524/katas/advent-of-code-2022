import { deepClone } from './common.js'
import { promises } from 'fs'

const getFirstMarker = async filePath => {
  const data = await promises.readFile(filePath, 'utf-8')
  return getFirstMarkerPosition(data)
}

const getFirstMessageMarker = async filePath => {
  const data = await promises.readFile(filePath, 'utf-8')
  return getFirstMarkerMessagePosition(data)
}

const getFirstMarkerPosition = (datastream) => {
  return getMarkerBySize(datastream, 4)
}

const getFirstMarkerMessagePosition = (datastream) => {
  return getMarkerBySize(datastream, 14)
}

const getMarkerBySize = (datastream, markerSize) => {
  const arrayData = datastream.split('')
  let markerNumber = -1
  arrayData.every((_, index, array) => {
    if (index >= array.length - (markerSize - 1)) {
      return false
    }
    const newElement = array.slice(index, index + markerSize)
    if (isAMarker(newElement.join(''))) {
      markerNumber = index + markerSize
      return false
    }
    return true
  })
  return markerNumber
}

const isAMarker = (inputLetters) => {
  return inputLetters.split('')
    .reduce((acc, newLetter, index, arrayOfLetters) => {
      const arrayToWork = deepClone(arrayOfLetters)
      arrayToWork.splice(index, 1)

      if (arrayToWork.includes(newLetter)) {
        acc = false
      }
      return acc
    }, true)
}

export { isAMarker, getFirstMarkerPosition, getFirstMarker, getFirstMarkerMessagePosition, getFirstMessageMarker }
