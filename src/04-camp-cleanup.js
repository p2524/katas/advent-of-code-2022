import { getIntersect } from './03-rucksack-reorganization.js'
import { getListOfLinesFromFile } from './common.js'

const getFullyOverlaps = async filePath => {
  const linesOfSections = await getListOfLinesFromFile(filePath)
  return getCoincidencesByMethod(linesOfSections, checkFullyContains)
}

const getOverlaps = async filePath => {
  const linesOfSections = await getListOfLinesFromFile(filePath)
  return getCoincidencesByMethod(linesOfSections, checkOverlap)
}

const getCoincidencesByMethod = (linesOfSections, checkMethod) => {
  return linesOfSections
    .map(getListsFromLine)
    .filter(([sectionsElf1, sectionsElf2]) => checkMethod(sectionsElf1, sectionsElf2))
    .length
}

const checkFullyContains = (list1, list2) => {
  const intersectList = getIntersect(list1, list2)
  return checkArrayEquals(intersectList, list1) || checkArrayEquals(intersectList, list2)
}

const checkOverlap = (list1, list2) => {
  return getIntersect(list1, list2).length > 0
}

const checkArrayEquals = (list1, list2) => {
  return JSON.stringify(list1) === JSON.stringify(list2)
}

const getListsFromLine = line => {
  return line.split(',').map(elfRange => {
    const [first, last] = elfRange.split('-')
    return range(parseInt(first), parseInt(last), 1)
  })
}

const range = (start, stop, step) =>
  Array.from({ length: (stop - start) / step + 1 }, (_, i) => start + i * step)

export { checkFullyContains, getListsFromLine, getFullyOverlaps, checkOverlap, getOverlaps }
