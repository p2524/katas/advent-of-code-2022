import { getListOfLinesFromFile } from './common.js'

// PART 1
const findDirectoriesBySize = (size, tree) => {
  const treeWithSizes = getSizeOfDirectories(tree)
  const newTreeWithSizes = mapTree(treeWithSizes, mapDirToSize, [])
  return {
    directories: newTreeWithSizes.filter(dir => dir.size <= size),
    total: newTreeWithSizes.filter(dir => dir.size <= size).reduce((acc, dir) => acc + dir.size, 0)
  }
}

// PART 2
const findSmallDirectoryToFreeSpace = (memory, tree) => {
  const treeWithSizes = getSizeOfDirectories(tree)
  const newTreeWithSizes = mapTree(treeWithSizes, mapDirToSize, [])
  const totalMemoryUsed = treeWithSizes.size
  const minimumSpaceForRemoval = totalMemoryUsed - (memory.total - memory.needFree)

  const directoriesForDeletion = newTreeWithSizes
    .filter(directory => directory.size >= minimumSpaceForRemoval)
    .sort(compareDirectories)
  return {
    name: directoriesForDeletion[0].name,
    size: directoriesForDeletion[0].size
  }
}

const getTreeFromFile = async (filePath) => {
  const commands = await getListOfLinesFromFile(filePath)

  const newTreeInReduce = commands.reduce((newTree, command) => {
    if (command.includes('$ cd')) {
      return processCommand(command, newTree)
    }
    if (!command.includes('$ ls')) { return processList([command], newTree) }
    return newTree
  }, {})

  const finalDirPositioned = getDirPositionedInTree(newTreeInReduce)
  finalDirPositioned.positioned = false
  newTreeInReduce.positioned = true

  return newTreeInReduce
}

const calculateSizeOfDir = directory => {
  return directory.childs.reduce((totalSize, elementInDirectory) => {
    if (elementInDirectory.type === 'dir') {
      return totalSize + calculateSizeOfDir(elementInDirectory)
    }
    return totalSize + elementInDirectory.size
  }, 0)
}

const getDirPositionedInTree = tree => {
  if (tree.positioned === true) {
    return tree
  }
  if (tree.childs) {
    for (const child of tree.childs) {
      if (getDirPositionedInTree(child) !== -1) {
        return getDirPositionedInTree(child)
      }
    }
  }
  return -1
}

const getParentOfDirPositionedInTree = (tree) => {
  if (tree.childs !== null && tree.childs !== undefined) {
    for (const child of tree.childs) {
      if (child.type === 'dir') {
        if (child.positioned === true) {
          return tree
        } else {
          if (getParentOfDirPositionedInTree(child) !== -1) {
            return getParentOfDirPositionedInTree(child)
          }
        }
      }
    }
  }
  return -1
}

const processCommand = (command, tree) => {
  const cdCommandType = getCommandType(command)
  const [,, dirName] = command.split(' ')
  const methodByCommandType = {
    'cd-first-command': () => createDefaultTree(),
    'cd-go-back': () => putParentAsPositioned(tree),
    'cd-directory': () => moveToDirectory(tree, dirName)
  }

  return methodByCommandType[cdCommandType]()
}

const createDefaultTree = () => ({
  positioned: true,
  name: '/',
  type: 'dir'
})

const putParentAsPositioned = tree => {
  const dirPositioned = getDirPositionedInTree(tree)
  const parent = getParentOfDirPositionedInTree(tree)
  parent.positioned = true
  dirPositioned.positioned = false
  return tree
}

const moveToDirectory = (tree, dirName) => {
  const dirPositioned = getDirPositionedInTree(tree)
  if (dirPositioned.childs) {
    const index = dirPositioned.childs.findIndex((element) => element.name === dirName)
    dirPositioned.childs[index].positioned = true
  }
  dirPositioned.positioned = false
  return tree
}

const getCommandType = command => {
  if (command === '$ cd /') {
    return 'cd-first-command'
  }
  const [,, dirName] = command.split(' ')
  if (dirName === '..') {
    return 'cd-go-back'
  }
  return 'cd-directory'
}

const processList = (listOfElements, tree) => {
  const childs = listOfElements.map(element => processElementInLs(element))
  const dirToAppendChilds = getDirPositionedInTree(tree)
  if (dirToAppendChilds.childs) {
    dirToAppendChilds.childs.push(childs[0])
  } else {
    dirToAppendChilds.childs = childs
  }
  return tree
}

const processElementInLs = element => {
  const partsOfElement = element.split(' ')
  if (partsOfElement[0] === 'dir') {
    return {
      type: 'dir',
      name: partsOfElement[1],
      positioned: false
    }
  }
  return {
    type: 'file',
    name: partsOfElement[1],
    size: parseInt(partsOfElement[0])
  }
}

const getSizeOfDirectories = (tree) => {
  const size = calculateSizeOfDirectory(tree)
  if (tree.childs !== null && tree.childs !== undefined) {
    tree.childs.map(child => {
      if (child.type === 'dir') {
        return getSizeOfDirectories(child)
      }
      return child
    })
  }
  tree.size = size
  return tree
}

const calculateSizeOfDirectory = tree => {
  let initialSize = 0

  if (tree.childs) {
    tree.childs.forEach(element => {
      if (element.type === 'file') {
        initialSize += element.size
      }
      if (element.type === 'dir') {
        initialSize += calculateSizeOfDirectory(element)
      }
    })
  }

  return initialSize
}

const mapTree = (tree, method, array = []) => {
  array.push(method(tree))
  for (const child of tree.childs) {
    if (child.type === 'dir') {
      mapTree(child, method, array)
    }
  }
  return array
}

const mapDirToSize = dir => {
  return {
    name: dir.name,
    size: dir.size
  }
}

const compareDirectories = (directoryA, directoryB) => {
  if (directoryA.size < directoryB.size) {
    return -1
  }
  return 1
}

export { getTreeFromFile, calculateSizeOfDir, processCommand, processList, getDirPositionedInTree, getSizeOfDirectories, findDirectoriesBySize, findSmallDirectoryToFreeSpace }
