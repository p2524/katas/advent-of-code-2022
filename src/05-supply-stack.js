import { deepClone, getListOfLinesFromFile } from './common.js'

const getTopCratesFromFile = async filePath => {
  const { initialStack, orders } = await parseFileStackAndOrders(filePath)

  return getCratesByCraterMethod({ initialStack, orders }, moveStack)
}

const getTopCrates9001FromFile = async filePath => {
  const { initialStack, orders } = await parseFileStackAndOrders(filePath)

  return getCratesByCraterMethod({ initialStack, orders }, moveStack9001)
}

const parseFileStackAndOrders = async (filePath) => {
  const linesOfFile = await getListOfLinesFromFile(filePath)
  return linesOfFile
    .filter(line => isOrderLine(line) || isStackLine(line))
    .reduce(reduceLine, { initialStack: {}, orders: [] })
}

const getCratesByCraterMethod = ({ initialStack, orders }, moveMethod) =>
  getTopCreatesFromStack(moveMethod(orders, initialStack))

const moveStack = (orders, initialStack) =>
  orders.reduce(processLine, deepClone(initialStack))

const moveStack9001 = (orders, initialStack) =>
  orders.reduce(process9001Line, deepClone(initialStack))

const processLine = (acc, { numberElements, lineNumberFrom, lineNumberTo }) => {
  for (let i = 0; i < numberElements; i++) {
    acc[lineNumberTo].unshift(acc[lineNumberFrom].shift())
  }
  return acc
}

const process9001Line = (acc, { numberElements, lineNumberFrom, lineNumberTo }) => {
  for (let i = 0; i < numberElements; i++) {
    const positionToChange = numberElements - i - 1
    acc[lineNumberTo].unshift(acc[lineNumberFrom][positionToChange])
    acc[lineNumberFrom].splice(positionToChange, 1)
  }
  return acc
}

const getOrderFromLine = (line) => {
  return line.split(' ')
    .filter((_, index) => index % 2 !== 0)
    .map(stringValue => parseInt(stringValue))
    .reduce(reduceOrderLine, {})
}

const reduceOrderLine = (acc, value, index) => {
  if (index === 0) {
    acc = { ...acc, numberElements: value }
  }
  if (index === 1) {
    acc = { ...acc, lineNumberFrom: value }
  }
  if (index === 2) {
    acc = { ...acc, lineNumberTo: value }
  }
  return acc
}

const BLANK_COLUMN = ' '

const reduceLineOfStack = (acc, value, index) => {
  if (value !== BLANK_COLUMN) {
    if (!acc[index + 1]) {
      acc[index + 1] = []
    }
    acc[index + 1].push(value)
  }
  return acc
}
const parseLineOfStacks = (line, initialStack) => {
  return line.split('')
    .filter((_, index) => (index - 1) % 4 === 0)
    .reduce(reduceLineOfStack, deepClone(initialStack))
}

const isStackLine = line => line.includes('[')
const isOrderLine = line => line.includes('move')

const reduceLine = (acc, line) => {
  if (isStackLine(line)) {
    acc.initialStack = parseLineOfStacks(line, acc.initialStack)
  }
  if (isOrderLine(line)) {
    acc.orders.push(getOrderFromLine(line))
  }
  return acc
}

const getTopCreatesFromStack = stack => {
  return Object.entries(stack).map(([_, listOfCrates]) => listOfCrates[0]).join('')
}

export { moveStack, moveStack9001, getOrderFromLine, parseLineOfStacks, getTopCrates9001FromFile, parseFileStackAndOrders, getTopCreatesFromStack, getTopCratesFromFile }
