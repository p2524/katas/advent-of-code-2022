import { promises } from 'fs'

const getMaxCaloriesFromFile = async filePath => {
  const elfsCalories = await getCaloriesFromFilePath(filePath)
  return Math.max(...elfsCalories)
}

const getCaloriesFromFilePath = async filePath => {
  const data = await promises.readFile(filePath, 'utf-8')
  return parseFileDataToArray(data)
}

const parseFileDataToArray = stringEntry => {
  const elfsCalories = stringEntry.split('\n\n')
  return elfsCalories.map(elfCalorie => {
    const calories = elfCalorie.split('\n')
    return calories.reduce((acc, newCalory) => acc + parseInt(newCalory), 0)
  })
}

const getThreeMaxCalories = async filePath => {
  const elfsCalories = await getCaloriesFromFilePath(filePath)
  let topThree = elfsCalories.sort((a, b) => a - b)
  topThree = topThree.slice(topThree.length - 3, topThree.length)
  return topThree.reduce((acc, number) => acc + number, 0)
}

export { getMaxCaloriesFromFile, parseFileDataToArray, getThreeMaxCalories }
