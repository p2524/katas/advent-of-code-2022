import { getListOfLinesFromFile } from './common.js'

const ROCK_C1 = 'A'
const PAPER_C1 = 'B'
const SCISSORS_C1 = 'C'
const ROCK_C2 = 'X'
const PAPER_C2 = 'Y'
const SCISSORS_C2 = 'Z'
const LOSE = 'X'
const DRAW = 'Y'
const WIN = 'Z'

const calculateRound = (c1, c2) => {
  return getRoundResult(c1, c2) + handValue[c2]
}

const getHandFromResult = (c1, result) => {
  const handResult = {
    [ROCK_C1]: {
      [LOSE]: SCISSORS_C2,
      [DRAW]: ROCK_C2,
      [WIN]: PAPER_C2
    },
    [PAPER_C1]: {
      [LOSE]: ROCK_C2,
      [DRAW]: PAPER_C2,
      [WIN]: SCISSORS_C2
    },
    [SCISSORS_C1]: {
      [LOSE]: PAPER_C2,
      [DRAW]: SCISSORS_C2,
      [WIN]: ROCK_C2
    }
  }
  return handResult[c1][result]
}

const calculateRound2 = (c1, result) => {
  const handToShow = getHandFromResult(c1, result)
  return calculateRound(c1, handToShow)
}

const handValue = {
  [ROCK_C2]: 1,
  [PAPER_C2]: 2,
  [SCISSORS_C2]: 3
}

const getRoundResult = (c1, c2) => {
  const posibleSituations = {
    [ROCK_C2]: {
      [ROCK_C1]: 3,
      [PAPER_C1]: 0,
      [SCISSORS_C1]: 6
    },
    [PAPER_C2]: {
      [ROCK_C1]: 6,
      [PAPER_C1]: 3,
      [SCISSORS_C1]: 0
    },
    [SCISSORS_C2]: {
      [ROCK_C1]: 0,
      [PAPER_C1]: 6,
      [SCISSORS_C1]: 3
    }
  }
  return posibleSituations[c2][c1]
}

const getListOfRoundsFromFile = async filePath => {
  const data = await getListOfLinesFromFile(filePath)
  return data.map(element => element.split(' '))
}

const getResultFromArrayOfRounds = (arrayOfRounds, calculateRoundFn = calculateRound) => {
  return arrayOfRounds.reduce((acc, newRound) => {
    return acc + calculateRoundFn(...newRound)
  }, 0)
}

const calculateResultFromFilePath = async (filePath, calculateRoundFn = calculateRound) => {
  const listOfRounds = await getListOfRoundsFromFile(filePath)
  return getResultFromArrayOfRounds(listOfRounds, calculateRoundFn)
}

export { calculateRound, getListOfRoundsFromFile, getResultFromArrayOfRounds, calculateResultFromFilePath, calculateRound2 }
