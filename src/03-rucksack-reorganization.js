import { getListOfLinesFromFile } from './common.js'

const getPrioritiesFromFile = async filePath => {
  const listOfElements = await getListOfLinesFromFile(filePath)
  return listOfElements.reduce((acc, lineOfElements) => acc + getPriorityFromOneLine(lineOfElements), 0)
}

const getBadgePrioritiesFromFilePath = async filePath => {
  const listOfElements = await getListOfLinesFromFile(filePath)
  const linesFromTeams = getSeparatedByThree(listOfElements)
  return linesFromTeams.reduce((acc, teamBadges) => acc + getValueForLetter(getBadgeFromElfs(teamBadges).join('')), 0)
}

const getIntersect = (list1, list2) => {
  return [...new Set(list1.filter(element => list2.includes(element)))]
}

const getListsFromString = lineOfElements => {
  const first = lineOfElements.substr(0, lineOfElements.length / 2).split('')
  const second = lineOfElements.substr(lineOfElements.length / 2, lineOfElements.length).split('')
  return [first, second]
}

const alphabetLower = [...Array(26).keys()].map(i => String.fromCharCode(i + 97))
const alphabetUpper = [...Array(26).keys()].map(i => String.fromCharCode(i + 65))

const entireAlphabet = [...alphabetLower, ...alphabetUpper]

const getValueForLetter = element => {
  return entireAlphabet.indexOf(element) + 1
}

const getIntersectionFromBag = (lineOfElements) => {
  return getIntersect(...getListsFromString(lineOfElements))
}

const getPriorityFromOneLine = (lineOfElements) => {
  const intersection = getIntersectionFromBag(lineOfElements)
  return intersection.reduce((acc, element) => acc + getValueForLetter(element), 0)
}

const getBadgeFromElfs = (teamBadges) => {
  const [first, second, third] = teamBadges
  if (first) {
    return getIntersect(getIntersect(first.split(''), second.split('')), third.split(''))
  }
  return []
}

const getSeparatedByThree = listOfElements => {
  const newArray = []
  let intermediateResult = []
  listOfElements.forEach((value, index) => {
    if (index % 3 === 0 && index !== 0) {
      newArray.push(intermediateResult)
      intermediateResult = []
    }
    intermediateResult.push(value)
  })
  newArray.push(intermediateResult)
  return newArray
}

export {
  getIntersect,
  getListsFromString,
  getIntersectionFromBag,
  getPriorityFromOneLine,
  getPrioritiesFromFile,
  getBadgeFromElfs,
  getBadgePrioritiesFromFilePath,
  getSeparatedByThree
}
