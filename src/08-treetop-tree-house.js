import { getListOfLinesFromFile } from './common.js'

const getVisibleTrees = async (filePath) => {
  const grid = await getGridFromFilePath(filePath)

  return grid.reduce(getTreesVisibility, 0)
}

const isTreeVisibleInLine = (lineOfTrees, position) => {
  return isInTheEdge(lineOfTrees, position) || isVisibleFromLeft(lineOfTrees, position) || isVisibleFromRight(lineOfTrees, position)
}

const getMaxScenicScore = async filePath => {
  const grid = await getGridFromFilePath(filePath)
  return grid.reduce(getMaxFromTrees, 0)
}

const getGridFromFilePath = async filePath => {
  const listOfElements = await getListOfLinesFromFile(filePath)
  return listOfElements.map(line => line.split('').map(value => parseInt(value)))
}

const getTreesVisibility = (acc, horizontalLineOfTrees, y, actualGrid) => {
  return acc + horizontalLineOfTrees.reduce(getHorizontalLineVisibility(actualGrid, y), 0)
}

const getHorizontalLineVisibility = (actualGrid, y) => (lineAcc, _, x, actualHorizontalLine) => {
  const verticalLineOfTrees = actualGrid.map(lineOfTrees => lineOfTrees[x])
  if (isTreeVisibleInGrid(actualHorizontalLine, verticalLineOfTrees, x, y)) {
    return lineAcc + 1
  }
  return lineAcc
}

const isTreeVisibleInGrid = (horizontalLine, verticalLine, x, y) => {
  return isTreeVisibleInLine(horizontalLine, x) || isTreeVisibleInLine(verticalLine, y)
}

const getMaxFromTrees = (maxValue, horizontalLineOfTrees, y, actualGrid) => {
  const maxInLine = horizontalLineOfTrees.reduce(getMaxFromLineOfTrees(actualGrid, y), 0)
  if (maxInLine >= maxValue) {
    return maxInLine
  }
  return maxValue
}

const getMaxFromLineOfTrees = (actualGrid, y) =>
  (lineMax, _, x, actualHorizontalLine) => {
    const verticalLineOfTrees = actualGrid.map(lineOfTrees => lineOfTrees[x])
    const scoreForTree = getScoreFromTree(verticalLineOfTrees, actualHorizontalLine, x, y)
    if (scoreForTree >= lineMax) {
      return scoreForTree
    }
    return lineMax
  }

const getScoreFromTree = (horizontalLine, verticalLine, x, y) => {
  if (isInTheEdge(horizontalLine, y) || isInTheEdge(verticalLine, x)) {
    return 0
  }
  return getScoreFromLine(horizontalLine, y) * getScoreFromLine(verticalLine, x)
}

const getScoreFromLine = (line, position) => {
  const leftPart = line.slice(0, position).reverse()
  const rigthPart = line.slice(position + 1)

  return getScore(rigthPart, line[position]) * getScore(leftPart, line[position])
}

const getScore = (line, value) => {
  if (line.findIndex(element => element >= value) !== -1) {
    return line.findIndex(element => element >= value) + 1
  }
  return line.length
}

const isVisibleFromLeft = (lineOfTrees, position) => {
  return isVisibleFromSide(lineOfTrees, position, 'left')
}

const isVisibleFromRight = (lineOfTrees, position) => {
  return isVisibleFromSide(lineOfTrees, position, 'right')
}

const isVisibleFromSide = (lineOfTrees, position, side) => {
  const methodBySide = {
    right: (_, index) => index < position,
    left: (_, index) => index > position
  }

  return lineOfTrees
    .filter(methodBySide[side])// remove trees from other side
    .findIndex(tree => tree >= lineOfTrees[position]) === -1 // No tre is higher
}

const isInTheEdge = (lineOfTrees, position) => {
  return position === 0 || position === lineOfTrees.lenght - 1
}

export { getVisibleTrees, isTreeVisibleInLine, getMaxScenicScore }
