import { promises } from 'fs'

const getListOfLinesFromFile = async filePath => {
  const data = await promises.readFile(filePath, 'utf-8')
  return data.split('\n')
}

const deepClone = object => JSON.parse(JSON.stringify(object))

export { getListOfLinesFromFile, deepClone }
